<?php
/**
 * Created by PhpStorm.
 * User: Asif
 * Date: 3/26/2018
 * Time: 4:42 PM
 */

namespace Pondit\Calculator\AreaCalculator;


class Triangle
{
    public $base;
    public $height;

    public function getArea()
    {
        return 1/2* $this->base * $this->height;
    }
}